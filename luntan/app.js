const net = require('net')
const fs = require('fs')

// 引入封装之后的 request
const Request = require('./request')
const { log } = require('./utils')

const routeIndex = require('./routes/index')
const routeUser = require('./routes/user')
const routeMessage = require('./routes/message')
const routeTodo = require('./routes/todo')

const error = (request=null, code=404) => {
    const e = {
        404: 'HTTP/1.1 404 NOT FOUND\r\n\r\n<h1>NOT FOUND</h1>',
    }
    const r = e[code] || ''
    return r
}

const responseFor = (raw, request) => {
    const route = {}
    // 将引入进来的 routeMapper 与 route 合并
    const routes = Object.assign(route, routeIndex, routeUser, routeMessage, routeTodo)

    // 获取 response 函数
    const response = routes[request.path] || error

    // 将 request 作为 response 的参数传出去, 这样每一个 response 都可以与对应的 request 挂钩
    const resp = response(request)
    return resp
}

const processRequest = (data, socket) => {
    const raw = data.toString('utf8')
    const request = new Request(raw)
    const ip = socket.localAddress
    log(`ip and request, ${ip}\n${raw}`)

    // 然后调用 responseFor, 根据 request 生成响应内容
    const response = responseFor(raw, request)
    socket.write(response)
    socket.destroy()
}

const run = (host='', port=3000) => {
    // 创建一个服务器
    const server = new net.Server()

    // 开启一个服务器监听连接
    server.listen(port, host, () => {
        const address = server.address()
        log(`listening server at http://${address.address}:${address.port}`)
    })

    server.on('connection', (s) => {
        s.on('data', (data) => {
            //接收到数据时，返回
            processRequest(data, s)
        })
    })

    server.on('error', (error) => {
        log('server error', error)
    })

    // 当服务器关闭时被触发
    server.on('close', () => {
        log('server closed')
    })
}

// 程序的入口
const __main = () => {
    run('127.0.0.1', 5000)
}


if (require.main === module) {
    __main()
}
