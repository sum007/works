const nunjucks = require('nunjucks')

const fs = require('fs')
const { log } = require('../utils')

const User = require('../models/user')

const session = {}

// 配置 loader, nunjucks 会从这个目录中加载模板
const loader = new nunjucks.FileSystemLoader('templates', {
    // noCache: true 是关闭缓存, 这样每次都会重新计算模板
    noCache: true,
})

// 用 loader 创建一个环境, 用这个环境可以读取模板文件
const env = new nunjucks.Environment(loader)

const currentUser = (request) => {
    const id = request.cookies.user || ''
    // const uid = router[id] || -1
    const username = session[id]
    const u = User.findOne('username', username)
    return u
}

// 读取 html 文件
const template = (path, data) => {
    const s = env.render(path, data)
    return s
}

const headerFromMapper = (mapper={}, code=200) => {
    let base = `HTTP/1.1 ${code} OK\r\n`
    const keys = Object.keys(mapper)
    const s = keys.map((k) => {
        const v = mapper[k]
        const h = `${k}: ${v}\r\n`
        return h
    }).join('')

    const header = base + s
    return header
}

// 图片的响应函数, 读取图片并生成响应返回
const static = (request) => {
    // 静态资源的处理, 读取图片并生成相应返回
    const filename = request.query.file || 'doge.gif'
    const path = `../static/${filename}`
    const body = fs.readFileSync(path)
    const header = headerFromMapper()
    const h = Buffer.from(header + '\r\n')
    const r = Buffer.concat([h, body])
    return r
}

// 重定向函数
const redirect = (url) => {
    // 浏览器在收到 302 响应的时候
    // 会自动在 HTTP header 里面找 Location 字段并获取一个 url
    // 然后自动请求新的 url
    const headers = {
        Location: url,
    }
    const header = headerFromMapper(headers, 302)
    const r=  header + '\r\n' + ''
    return r
}

// 检测是否登录的函数
// 装饰器模式
// @login_required
const loginRequired = (routeFunc) => {
    const func = (request) => {
        const u = currentUser(request)
        if (u === null) {
            return redirect('/login')
        } else {
            return routeFunc(request)
        }
    }
    return func
}

module.exports = {
    session: session,
    currentUser: currentUser,
    template: template,
    headerFromMapper: headerFromMapper,
    static: static,
    redirect: redirect,
    loginRequired: loginRequired,
}