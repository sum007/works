const { log, randomStr } = require('../utils')
const {
    session,
    currentUser,
    template,
    loginRequired,
    headerFromMapper,
} = require('./main')


const User = require('../models/user')

// 登录的处理函数, 根据请求方法来处理业务
// 请求原始信息, path 是 /login
const login = (request) => {
    const headers = {
        'Content-Type': 'text/html',
    }
    let result
    if (request.method === 'POST') {
        // 获取表单中的数据
        const form = request.form()
        if (User.validateLogin(form)) {
            const u = User.create(form)
            u.save()
            // 这个时候 u 就有 id 了
            const sid = randomStr()
            // uvbmsn6e6bfdjag6
            session[sid] = u.username
            headers['Set-Cookie'] = `user=${sid}`
            result = '登录成功'
        } else {
            result = '用户名或者密码错误'
        }
    } else {
        result = ''
    }
    const u = currentUser(request)
    let username
    if (u === null) {
        username = ''
    } else {
        username = u.username
    }

    const body = template('login.html', {
        username: username,
        result: result,
    })
    const header = headerFromMapper(headers)
    const r = header + '\r\n' + body
    return r
}

// 注册的处理函数
const register = (request) => {
    let result
    if (request.method === 'POST') {
        const form = request.form()
        if (User.validateRegister(form)) {
            const u = User.create(form)
            u.save()
            result = `注册成功`
        } else {
            result = '用户名和密码长度必须大于2或者用户名已经存在'
        }
    } else {
        result = ''
    }
    const us = User.all()
    const body = template('register.html', {
        result: result,
        users: us
    })
    const headers = {
        'Content-Type': 'text/html',
    }
    const header = headerFromMapper(headers)
    const r = header + '\r\n' + body
    return r
}

const profile = (request) => {
    const headers = {
        'Content-Type': 'text/html',
    }
    const header = headerFromMapper(headers)
    const u = currentUser(request)
    const body = template('profile.html', {
        user: u,
    })
    const r = header + '\r\n' + body
    return r
}

const routeUser = {
    '/login': login,
    '/register': register,
    '/profile': loginRequired(profile),
}

module.exports = routeUser