const Model = require('./main')

class User extends Model {
    constructor(form={}) {
        super()
        this.id = form.id
        this.username = form.username || ''
        this.password = form.password || ''
        this.note = form.note || ''
    }

    static validateLogin(form={}) {
        const { username, password } = form
        const u = User.findOne('username', username)
        return u !== null && u.password === password
    }

    static validateRegister(form={}) {
        const { username, password } = form
        const validForm = username.length > 2 && password.length > 2
        const uniqueUser = User.findOne('username', username) === null
        return validForm && uniqueUser
    }
}

const test = () => {

}

// 当 nodejs 直接运行一个文件时, require.main 会被设为它的 module
// 所以可以通过如下检测确定一个文件是否直接运行
if (require.main === module) {
    test()
}

module.exports = User