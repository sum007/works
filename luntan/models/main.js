// 引入模块
const fs = require('fs')

// 一个辅助函数, 确保要操作的文件已经存在
const ensureExists = (path) => {
    if (!fs.existsSync(path)) {
        // 因为保存的数据都是 json 格式的, 所以在初始化文件的时候
        // 会写入一个空数组
        const data = '[]'
        fs.writeFileSync(path, data)
    }
}

// data 是 object 或者 array
// path 是 保存文件的路径
const save = (data, path) => {
    const s = JSON.stringify(data, null, 2)
    fs.writeFileSync(path, s)
}

const load = (path) => {
    const options = {
        encoding: 'utf8',
    }
    ensureExists(path)
    // 如果指定了 encoding, readFileSync 返回的就不是 buffer, 而是字符串
    const s = fs.readFileSync(path, options)
    const data = JSON.parse(s)
    return data
}

// 定义一个 Model 类来处理数据相关的操作
// Model 是基类, 可以被其他类继承
class Model {
    static dbPath() {
        const classname = this.name.toLowerCase()
        // db 的文件名通过这样的方式与类名关联在一起
        const path = require('path')
        const filename = `${classname}.txt`
        const p = path.join(__dirname, '../db', filename)
        return p
    }

    // 这个函数是用来获取一个类的所有实例
    static all() {
        const path = this.dbPath()
        const models = load(path)
        const ms = models.map((item) => {
            const cls = this
            const instance = cls.create(item)
            return instance
        })
        return ms
        // return models.map(m => this.create(m))
    }

    static create(form={}) {
        const cls = this
        const instance = new cls(form)
        return instance
    }

    static findOne(key, value) {
        const all = this.all()
        // es6 里新增的 find 方法, 可以把符合条件的元素查找出来
        // 如果没有找到, 返回的是 undefined
        let m = all.find((e) => {
            return e[key] === value
        })

        // 如果 m 为 undefined, 说明找不到符合条件的实例, 那就返回 null
        if (m === undefined) {
            m = null
        }
        return m

        // return all.find(e => e[key] === value) || null
    }

    // 查找 key 为 value 的所有实例
    static find(key, value) {
        const all = this.all()
        const models = all.filter((m) => {
            return m[key] === value
        })
        return models
    }

    static get(id) {
        return this.findOne('id', id)
    }

    save() {
        const cls = this.constructor
        const models = cls.all()
        if (this.id === undefined) {
            // 如果 id 不存在, 说明数据文件中没有当前这条数据
            if (models.length > 0) {
                const last = models[models.length - 1]
                this.id = last.id + 1
            } else {
                this.id = 0
            }
            models.push(this)
        } else {
            // id 存在说明这条数据已经在数据文件中了
            const index = models.findIndex((e) => {
                return e.id === this.id
            })
            if (index > -1) {
                models[index] = this
            }
        }
        const path = cls.dbPath()
        save(models, path)
    }

    static remove(id) {
        const cls = this
        const models = cls.all()
        const index = models.findIndex((e) => {
            return e.id === id
        })
        if (index > -1) {
            models.splice(index, 1)
        }
        const path = cls.dbPath()
        save(models, path)
    }

    toString() {
        const s = JSON.stringify(this, null, 2)
        return s
    }
}

module.exports = Model